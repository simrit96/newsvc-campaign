from http import HTTPStatus
from urllib.parse import urlencode

import requests

class BaseApiResource:
    def __init__(self, base_url: str, auth_token: str = None, *args, **kwargs):
        self.base_url = base_url
        self.auth_token = auth_token
        self.headers = kwargs.get("headers", {})
        self.auth_type = kwargs.get("auth_type", "Bearer")
        self.available_request_method = ["get", "post", "put", "delete"]
        self.http_status_mapping = {status.value: status for status in HTTPStatus}

    def generate_auth_header(self):
        if self.auth_type == "Bearer":
            if not str(self.auth_token).startswith(self.auth_type):
                self.auth_token = f"{self.auth_type} {self.auth_token}"

            self.headers["Authorization"] = self.auth_token

    def make_request(
        self, request_method: str, path: str, payload: dict = {}, params: dict = {}
    ):
        if request_method not in self.available_request_method:
            return HTTPStatus.METHOD_NOT_ALLOWED, {"message": "Method not allowed"}

        request_object = getattr(requests, request_method)
        self.generate_auth_header()

        if isinstance(params, dict):
            params = urlencode(params)  # type: ignore

        resp = request_object(
            url=f"{self.base_url}{path}?{params}", json=payload, headers=self.headers
        )

        try:
            response_data = resp.json()
        except Exception:
            response_data = resp.text

        return self.http_status_mapping.get(resp.status_code), response_data
