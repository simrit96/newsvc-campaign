from . import BaseApiResource


class AuthResource(BaseApiResource):
    def __init__(self, base_url: str, auth_token: str):
        self.base_url = base_url
        self.auth_token = auth_token
        super(AuthResource, self).__init__(
            base_url=self.base_url, auth_token=self.auth_token
        )

    def validate_user_token(self, auth_token: str = None):
        path = "/validate"

        payload = dict(auth_token=auth_token)

        status, result = self.make_request(
            request_method="post", path=path, payload=payload
        )

        return status, result

    def validate_server_token(self, auth_token: str = None):
        path = "/servers/validate"

        payload = dict(auth_token=auth_token)

        status, result = self.make_request(
            request_method="post", path=path, payload=payload
        )

        return status, result

    def validate_partner_token(self, auth_token: str = None):
        path = "/partners/validate"

        payload = dict(auth_token=auth_token)

        status, result = self.make_request(
            request_method="post", path=path, payload=payload
        )

        return status, result

    def validate_admin_token(self, auth_token: str = None):
        path = "/admin/validate"

        payload = dict(auth_token=auth_token)

        status, result = self.make_request(
            request_method="post", path=path, payload=payload
        )

        return status, result

    def validate_general_token(
        self, auth_token: str, token_types: list = [], return_permission: bool = False
    ):
        path = "/validate/token"
        payload = dict(
            auth_token=auth_token,
            token_types=token_types,
            return_permission=return_permission,
        )

        status, result = self.make_request(
            request_method="post", path=path, payload=payload
        )
        return status, result