import json
from http import HTTPStatus
from typing import Union

from flask import Response


def make_json_response(http_status: Union[HTTPStatus, int], data: dict) -> Response:
    """
    This method used to make flask Response object from given http status and response data

    Arguments:
        http_status {Union[HTTPStatus, int]} -- The HTTP status to be returned
        data {dict} -- The response data

    Returns:
        Response -- flask Response object
    """
    return Response(
        response=json.dumps(data), status=http_status, mimetype="application/json"
    )


def unauthorized(errors: dict = {}) -> Response:
    """
    This method used to make unauthorized API response

    Returns:
        Response -- flask Response object
    """
    data = {"message": "Unauthorized", "code": 401, "errors": errors}
    return make_json_response(http_status=401, data=data)
