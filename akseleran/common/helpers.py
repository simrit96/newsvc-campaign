import http
from akseleran.common.models import Result


def split_token(headers: dict) -> Result:
    """
    get Authorization header and split the token
    """
    allowed_auth_type = ["bearer", "jwt"]
    auth_header = headers.get("Authorization", headers.get("authorization"))
    auth_type, auth_token = (
        auth_header.split() if auth_header is not None else (None, None)
    )

    if not all([auth_type, auth_token]) or auth_type.lower() not in allowed_auth_type:
        return Result(
            status_code=http.HTTPStatus.UNAUTHORIZED,
            data=None,
            message="No header or authentication not valid",
        )

    return Result(status_code=http.HTTPStatus.OK, data=auth_token, message=None)
