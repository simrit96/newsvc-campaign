"""
This class used to define model for akseleran
"""

import http
from collections import namedtuple

class Result(namedtuple("Result", "status_code data message")):
    __slots__ = ()

    @property
    def success(self):
        return (
            True
            if self.status_code
            in [http.HTTPStatus.OK, http.HTTPStatus.CREATED, http.HTTPStatus.NO_CONTENT]
            else False
        )
