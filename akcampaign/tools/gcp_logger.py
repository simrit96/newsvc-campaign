import logging
import google.cloud.logging
from google.cloud.logging.handlers import CloudLoggingHandler


class GCPLogging:
    gcp_client = google.cloud.logging.Client()
    gcp_client.setup_logging()
    gcp_handler = CloudLoggingHandler(gcp_client, name="akcampaign")
    logger = logging.getLogger("akcampaign")
    logger.addHandler(gcp_handler)

    gcp_client.setup_logging()

    def error(data, message):
        GCPLogging.logger.error(data)
        GCPLogging.logger.error(message)

    def info(message):
        GCPLogging.logger.info(message)
