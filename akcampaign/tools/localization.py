import gettext
import os

from akcampaign.tools.gcp_logger import GCPLogging


class Localization:
    def translate(word=None, lang="id"):
        try:
            if lang not in ["id", "en"]:
                lang = "id"
            here = os.path.abspath(os.path.dirname(__file__))
            localedir = os.path.join(here, os.pardir, "localization")
            locale = gettext.translation("base", localedir=localedir, languages=[lang])
            locale.install()
            _ = locale.gettext
            translation = _(word)

            return translation
        except Exception as e:
            GCPLogging.error(e)
