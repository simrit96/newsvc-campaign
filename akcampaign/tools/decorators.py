from functools import wraps

from akseleran.common import split_token
from akseleran.helpers.http_responses import unauthorized
from akseleran.internal.restapi.auth import AuthResource
from flask import current_app, g, request


def login_required(_func=None, *, permissions=[], token_types=[]):  # noqa
    def decorator_login_required(func):
        @wraps(func)
        def wrap(*args, **kwargs):
            auth_resource = AuthResource(
                base_url= "https://api.devcloud6.akseleran.com/auth",
                auth_token=None,
            )
            if request.headers.get("Authorization") is None:
                return unauthorized(errors=dict(token="Invalid token"))
            try:
                auth_token = None
                token_result = split_token(request.headers)
                if token_result.status_code == 200:
                    auth_token = token_result.data
                else:
                    return unauthorized(errors=dict(token="Invalid token"))

                (
                    validation_status,
                    validation_result,
                ) = auth_resource.validate_general_token(
                    auth_token=auth_token,
                    token_types=token_types,
                    return_permission=True,
                )
            except (TypeError, UnicodeDecodeError, ValueError):
                return unauthorized(errors=dict(token="Invalid token"))

            if validation_status == 200:
                validation_data = validation_result.get("data", {})
                user_permissions = validation_data.get("permissions")

                if not validation_data.get("token_type") == "SERVER":
                    for permission in permissions:
                        if permission not in user_permissions:
                            return unauthorized(errors=dict(token="Access forbidden"))

                token_type = validation_data.get("token_type")
                if token_type == "ADMIN":
                    g.admin_id = int(validation_data.get("user_id"))
                elif token_type == "USER":
                    g.user_id = int(validation_data.get("user_id"))
                elif token_type == "PARTNER":
                    g.partner_id = int(validation_data.get("partner_id"))

                return func(*args, **kwargs)

            return unauthorized(errors=dict(token="Invalid token"))

        return wrap

    if _func is None:
        return decorator_login_required
    else:
        return decorator_login_required(_func)
