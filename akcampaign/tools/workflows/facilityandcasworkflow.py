from google.cloud import workflows_v1beta
from google.cloud.workflows import executions_v1beta
from google.cloud.workflows.executions_v1beta.types import executions, Execution
import json
import time

from akcampaign.tools.gcp_logger import GCPLogging

def callWorkflow(workflowDetails = {}, payload = {}):

    try:
        project = workflowDetails.get('project')
        location = workflowDetails.get('location')
        workflow = workflowDetails.get('workflow')

        execution = Execution(argument = json.dumps(payload))

        execution_client = executions_v1beta.services.executions.ExecutionsClient()

        workflows_client = workflows_v1beta.WorkflowsClient()

        parent = workflows_client.workflow_path(project, location, workflow)

        response = execution_client.create_execution(parent= parent, execution = execution)

        #print(f"Created execution: {response.name}")

        execution_finished = False
        backoff_delay = 1  # Start wait with delay of 1 second

        while (not execution_finished):
            execution = execution_client.get_execution(request={"name": response.name})
            execution_finished = execution.state != executions.Execution.State.ACTIVE

            # If we haven't seen the result yet, wait a second.
            # if not execution_finished:
            #     print('- Waiting for results...')
            # time.sleep(backoff_delay)
            # backoff_delay *= 2  # Double the delay to provide exponential backoff.
        else:
            # print(f'Execution finished with state: {execution.state.name}')
            response = json.loads(execution.result)
            return response

    except Exception as e:
        GCPLogging.error(e, "Failed to execute FacilityAndCasWorkflow")
        return None