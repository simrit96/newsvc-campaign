class UserTools:
    @staticmethod
    def get_type_name(user_type=None, full_name=None, nama_badan=None):
        try:
            user_name = None
            salutation = None

            if user_type.lower() == "individual":
                user_name = full_name
                salutation = "Bapak/Ibu"
            elif user_type.lower() == "company":
                user_name = nama_badan
                salutation = ""

            if user_name is not None:
                user_name = user_name.title()
            else:
                user_name = user_name

            return salutation, user_name
        except Exception as e:
            raise Exception(e)
