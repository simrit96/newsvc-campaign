from flask import Blueprint
from flask_restful import Api
from akcampaign.resources.campaigns import CampaignCategoriesResource, FeaturedCampaignResource, CampaignResource,CampaignDetailsResource, CreateCampaignsResource

categories_blueprint = Blueprint('campaign_categories', __name__, url_prefix = '/categories')

featured_campaigns_blueprint = Blueprint('featured_campaigns', __name__, url_prefix = '/featured')

campaigns_blueprint = Blueprint("campaigns", __name__, url_prefix = "/")


# admin_campaigns_blueprint = Blueprint("admin_campaigns", __name__,
# url_prefix = "/admin")

categories_api_resource = Api(categories_blueprint)
categories_api_resource.add_resource(CampaignCategoriesResource, "")

featured_campaigns_resource = Api(featured_campaigns_blueprint)
featured_campaigns_resource.add_resource(FeaturedCampaignResource, "")

campaigns_blueprint_resource = Api(campaigns_blueprint)
campaigns_blueprint_resource.add_resource(CampaignResource, "")
campaigns_blueprint_resource.add_resource(CampaignDetailsResource, "/<string:uuid>")
campaigns_blueprint_resource.add_resource(CreateCampaignsResource, "/create/<string:uuid>")

# admin_campaigns_blueprint_resource = Api(admin_campaigns_blueprint)
# admin_campaigns_blueprint_resource.add_resource(AdminCampaigns, "")
# admin_campaigns_blueprint_resource.add_resource(AdminCampaignDetailsResource, "/<string:uuid>")