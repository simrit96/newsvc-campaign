from flask_restful import Resource
from flask import request, current_app

from akcampaign.schemas.campaigns import CampaignCategoriesRequestSchema, CampaignCategoriesResponseSchema, FeaturedCampaignsRequestSchema, CampaignSchema, CampaignCreditRatingsSchema, CampaignListRequestSchema, CampaignDetailsSchema,CampaignDetailsModelRequestSchema, CampaignPartnerModelSchema, CampaignInstitutionalLenderSchema, CampaignType, CampaignPaymentListRequestSchema

from akcampaign.models.campaigns import NewCampaigns, NewCampaignsDetail


from akcampaign.schemas.loans import LoanRequestSchema, LoanResponseSchema, LoanDetailsResponseSchema, LoanCreditRatingSchema

from akcampaign.controllers.campaigns import get_featured_funding_details, get_campaigns_featured, get_campaign, get_final_campaign_details, get_campaign_admin, get_campaign_details_admin

from akcampaign.controllers.loans import translateLoanRequestToCampaign, LoanRequestDetailsToCampaignDetails

from akcampaign.tools.users import UserTools
from akcampaign.tools.gcp_logger import GCPLogging
from akcampaign.tools.workflows.facilityandcasworkflow import callWorkflow

from akcampaign.tools.decorators import login_required

class CampaignCategoriesResource(Resource):

    def get(self):
        try:
            res = []
            categories = CampaignCategoriesRequestSchema().get_categories()
            for category in categories.get("campaign_categories"):
                res.append(CampaignCategoriesResponseSchema().dump(category))

            return res

        except Exception as e:
            GCPLogging.error(e, "Exception occured while trying to get categories")
            return res


class FeaturedCampaignResource(Resource):

    def get(self):
        res = []
        args = request.args.to_dict()
        try:
            featured_campaigns_list = FeaturedCampaignsRequestSchema().get_featured_campaigns(args)
            for campaign in featured_campaigns_list.get('featured_campaigns'):
                campaign_data = CampaignSchema().dump(campaign)
                
                campaign_credit_rating = CampaignCreditRatingsSchema().get_credit_rating(campaign.loan_credit_rating_id)

                campaign_credit_rating_data = CampaignCreditRatingsSchema(only = ['credit_rating']).dump(campaign_credit_rating.get("credit_rating"))

                campaign_investments_data = get_featured_funding_details(campaign.campaign_investment, campaign.max_funding)


                campaign_detail_data = {
                    "have_collateral": campaign.campaign_detail.have_collateral,
                    "business_description": campaign.campaign_detail.business_description
                }
                data = get_campaigns_featured(campaign_data, campaign_credit_rating_data, campaign_investments_data, campaign_detail_data)
                res.append(data)

            return {
                'loan' : res
            }, 200


        except Exception as e:
            GCPLogging.error(e, 'Exception occured while trying to get featured campaigns')
            return None


class CampaignResource(Resource):

    def get(self):
        args = request.args.to_dict()
        campaigns_list = CampaignListRequestSchema().get_all_campaigns(args)
        campaign_list_data = []

        for campaign in campaigns_list.get("campaigns"):
            campaign_data = CampaignSchema().dump(campaign)

            campaign_credit_rating = CampaignCreditRatingsSchema().get_credit_rating(campaign.loan_credit_rating_id)

            campaign_credit_rating_data = CampaignCreditRatingsSchema(only = ['credit_rating']).dump(campaign_credit_rating.get("credit_rating"))

            campaign_investments_data = get_featured_funding_details(campaign.campaign_investment, campaign.max_funding)
            
            campaign_detail_data = CampaignDetailsSchema().dump(campaign.campaign_detail)

            data = get_campaign(campaign_data, campaign_credit_rating_data, campaign_investments_data, campaign_detail_data, campaign.campaign_type)
            campaign_list_data.append(data)


        try:
            result = {
                "data": campaign_list_data,
                "total": campaigns_list.get("count"),
                "offset": args.get("offset"),
                "limit": args.get("limit"),
            }
        except Exception as e:
            GCPLogging.error(e, "Failed to get campaigns")
        return result , 200


class CampaignDetailsResource(Resource):

    def get(self, uuid):
        try:
            campaign_details_data = CampaignDetailsModelRequestSchema().get_campaign_details(uuid)
            
            if campaign_details_data.get("id") == 0:
                GCPLogging.error(campaign_details_data.get('id'))
                return {"code": 404, "message": "Campagin Not Found"}, 404

            lang_data = {
            "lang": "id",
            }
        
            campaign_credit_rating = CampaignCreditRatingsSchema().get_credit_rating(campaign_details_data.get("loan_credit_rating_id"))


            campaign_partner = CampaignPartnerModelSchema().get_partner(campaign_details_data['campaign_details'].get('partner_id'))

            head_coborrower_initial = '-'

            campaign_details_data["campaign"]["head_coborrower_initial"] = head_coborrower_initial

            data = dict()
            data['campaign_uuid'] = uuid
            data['user_id'] = campaign_details_data['campaign'].get('user_id')

            auth_token = current_app.config.get("AKCAMPAIGN_FACILITY_SERVER_TOKEN")
            
            data['token'] = "Bearer " + auth_token

            workflowDetails = dict()
            workflowDetails['project'] = "aksl-runtime-dev"
            workflowDetails['location'] = "asia-southeast1"
            workflowDetails['workflow'] = "FacilityAndCasWorkflow"

            try:
                workflow_data_response = callWorkflow(workflowDetails, data)
                if workflow_data_response is not None:
                    cas_response = workflow_data_response[1]
                    facility_service_response = workflow_data_response[0]
                else:
                    cas_response = None
                    facility_service_response = None

                if 'body' in cas_response and 'head_coborrower_initial' in cas_response.get('body'):
                    head_coborrower_initial = cas_response['body'].get('head_coborrower_initial')
                    
                    campaign_details_data["campaign"]["head_coborrower_initial"] = head_coborrower_initial

                online_highlight = None

                if 'code' in facility_service_response and facility_service_response.get('code') == 200:
                    data = facility_service_response['body'].get('data')
                    if data is not None:
                        online_highlight = data.get('online_merchant_highlight')


                if online_highlight is not None:
                    campaign_details_data["campaign_details"]["online_merchant_highlight"] = online_highlight

            except Exception as e:
                GCPLogging.error(e, "Failed to execute facilityCasWorkflow")
                return None

            result = get_final_campaign_details(campaign_details_data["campaign"], campaign_details_data["campaign_details"], campaign_details_data["investments"], campaign_credit_rating,campaign_partner,lang_data)

            return result, 200

        except Exception as e:
            GCPLogging.error(e, "Failed to prepare final campaign data for details")
            return None


class CreateCampaignsResource(Resource):

    @login_required(token_types=["ADMIN"])
    def put(self, uuid):
        try:
            data = {}
            data = request.get_json()
            data.update(dict(uuid = uuid))
            loan_request_data = LoanRequestSchema().getLoanRequests(**data)

            if loan_request_data is None:
                return {"message": "Loan Id Not Found", "code": 404}, 404

            loan_request_details = loan_request_data.get('loan_request').details
            loan_request_dict = LoanResponseSchema().dump(loan_request_data.get('loan_request'))
            
            # Add credit rating column
            loan_credit_rating = LoanCreditRatingSchema().get_credit_rating(loan_request_data.get('loan_request').credit_rating_id)

            
            loan_credit_rating_dict = LoanCreditRatingSchema().dump(loan_credit_rating.get('credit_rating'))

            loan_request_dict['credit_rating'] = loan_credit_rating_dict.get('credit_rating')

            loan_request_details_dict = LoanDetailsResponseSchema().dump(loan_request_details)
            
            campaign_data = translateLoanRequestToCampaign(loan_request_dict)

            loan_request_details_dict['partner_id'] = loan_request_dict.get('partner_id')
            loan_request_details_dict['business_description'] = loan_request_dict.get('business_description')
            loan_request_details_dict['collateral_type'] = loan_request_dict.get('collateral_type')
            loan_request_details_dict['collateral_description'] = loan_request_dict.get('collateral_description')

            loan_request_details_dict['id'] = loan_request_dict.get('id')

            campaign_details_data = LoanRequestDetailsToCampaignDetails(loan_request_details_dict)
            campain_obj = NewCampaigns(**campaign_data)
            campain_obj.save()
            campain_details_obj = NewCampaignsDetail(**campaign_details_data)
            campain_details_obj.save()

        except Exception as e:
            GCPLogging.error(e, "Failed to prepare data for campaign creation")
            return None


# class AdminCampaigns(Resource):

#     @login_required(token_types=["ADMIN"])
#     def get(self):
#         try:
#             data = request.args.to_dict()

#             campaigns_list = CampaignListRequestSchema().get_all_campaigns(data)

#             campaigns_list_data = []

#             if len(campaigns_list) > 0:
#                 user_id_set = set()

#                 for campaign in campaigns_list["campaigns"]:
#                     if campaign.created_by:
#                         user_id_set.add(campaign.created_by)

#                     if campaign.user_id:
#                         user_id_set.add(campaign.user_id)

#                     elif campaign.user_group_id:
#                         user_id_set.update(campaign.user_group_id)


#                 user_id_list = list(user_id_set)

#                 user_id_list = [str(co) for co in user_id_list]

#                 user_data_dict = {}

#                 data = {}

#                 if request.headers.get('authorization') is not None:
#                     token_split = request.headers.get('authorization').split(" ")

#                 else:
#                     return {"code": 401, "message": "Unauthorized"}, 401

#                 data['token'] = "Bearer " + token_split[1]
#                 data['userInputId'] = user_id_list
#                 workflowDetails = dict()
#                 workflowDetails['project'] = "aksl-runtime-dev"
#                 workflowDetails['location'] = "us-central1"
#                 workflowDetails['workflow'] = "UserServiceWorkflow"


#                 workflow_data_response = callWorkflow(workflowDetails, data)

#                 if workflow_data_response.get('code') == 200:
#                     for user_data in workflow_data_response["data"].get("users"):
#                         user_data_dict[user_data.get("id")] = user_data

#             for campaign in campaigns_list["campaigns"]:
#                 campaign_data = CampaignSchema().dump(campaign)

#                 campaign_credit_rating = CampaignCreditRatingsSchema().get_credit_rating(campaign.loan_credit_rating_id)

#                 campaign_credit_rating_data = CampaignCreditRatingsSchema(only = ['credit_rating']).dump(campaign_credit_rating["credit_rating"])

#                 campaign_investments_data = get_featured_funding_details(campaign.campaign_investment, campaign.max_funding)

#                 campaign_detail_data = CampaignDetailsSchema().dump(campaign.campaign_detail)

#                 campaign_instituional_lender = CampaignInstitutionalLenderSchema().get_institutional_lender(campaign.id)

#                 if campaign.user_id:
#                     user = user_data_dict.get(campaign.user_id)
#                 elif (campaign.campaign_type == CampaignType.P2P_EMPLOYEE_LOAN and campaign.user_group_id):
#                     user = user_data_dict.get(campaign.campaign_user_group.user_id)

#                 if user:
#                     salutation, campaign_owner = UserTools().get_type_name(
#                         user_type=user["user_type"]["value"],
#                         full_name=user.get("full_name"),
#                         nama_badan=user.get("company_name"),
#                     )
#                 campaign_data["campaign_owner"] = campaign_owner

#                 if campaign.created_by:
#                     user = user_data_dict.get(campaign.created_by)
#                     salutation, campaign_creator = UserTools().get_type_name(
#                         user_type=user["user_type"]["value"],
#                         full_name=user.get("full_name"),
#                         nama_badan=user.get("company_name"),
#                     )
#                 campaign_data["campaign_creator_name"] = campaign_creator

#                 data = get_campaign_admin(
#                     campaign_data,
#                     campaign_detail_data,
#                     campaign_investments_data,
#                     campaign_instituional_lender,
#                     campaign_credit_rating_data,
#                 )

#                 campaigns_list_data.append(data)

#             result = {
#                     "campaign_list": campaigns_list_data,
#                     "total": campaigns_list["count"],
#                     "offset": data["offset"],
#                     "limit": data["limit"],
#                 }
#             return result, 200

#         except Exception as e:
#             GCPLogging.error(e, "Failed to get Admin Campaigns")
#             return None


# class AdminCampaignDetailsResource(Resource):

#     @login_required(token_types=["ADMIN"])
#     def get(self, uuid):
#         try:
#             data = CampaignDetailsModelRequestSchema().get_campaign_details(uuid)

#             if data.get("id") == 0:
#                 return {"code": 404, "message": "Campaign not found"}, 404

#             try:
#                 campaign_credit_rating = CampaignCreditRatingsSchema().get_credit_rating(data.get('loan_credit_rating_id'))
#             except Exception:
#                 pass

#             try:
#                 campaign_partner = CampaignPartnerModelSchema().get_partner(data.get('campaign_details').get('partner_id'))
#             except Exception:
#                 pass

#             try:
#                 campaign_payment_list = CampaignPaymentListRequestSchema().get_payment_list(data.get("id"))
#             except Exception:
#                 pass


#             result = get_campaign_details_admin(data["campaign"],
#             data["campaign_details"], data["investments"], data["campaign_institutional_lender"], campaign_credit_rating,campaign_partner, campaign_payment_list)

#             return result, 200

#         except Exception as e:
#             GCPLogging.error(e, "Failed to get admin campaign details")
#             return None
