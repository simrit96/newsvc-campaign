from flask import Flask, Response
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
import json
import http
from logging import getLogger
from flask_migrate import Migrate


from akcampaign.tools.gcp_logger import GCPLogging

from akcampaign import config

db = SQLAlchemy()
ma = Marshmallow()
migrate = Migrate()

error_logger = getLogger("error")
app_logger = getLogger("app")


def exception_handler(e: Exception) -> Response:
    error_logger.exception(e)
    GCPLogging.error(e, "Internal Server Error")

    return Response(
        response=json.dumps({"code": 500, "message": "Internal Server Error"}),
        status=http.HTTPStatus.INTERNAL_SERVER_ERROR,
        mimetype="application/json",
    )


def create_app():
    app = Flask(__name__)
    app.config.from_object(config.Config)
    db.init_app(app)
    ma.init_app(app)
    migrate.init_app(app, db)
    from akcampaign.blueprints.campaigns import categories_blueprint,featured_campaigns_blueprint, campaigns_blueprint

    with app.app_context():
        app.register_blueprint(categories_blueprint)
        app.register_blueprint(featured_campaigns_blueprint)
        app.register_blueprint(campaigns_blueprint)
        # app.register_blueprint(admin_campaigns_blueprint)

        app.register_error_handler(Exception, exception_handler)
    return app
