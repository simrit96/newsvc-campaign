def get_featured_funding_details(data, max_funding):
    total_investment = 0
    count = 0

    for investment in data:
        if investment.is_deleted is False:
            total_investment += investment.amount
            count += 1

    try:
        funded_percentage = round(
            (total_investment / max_funding) * 100, 2
        )
    except Exception:
        funded_percentage = None

    response = {
        "funded_percentage": funded_percentage,
        "total_investors": count,
        "total_investment": total_investment
    }

    return response

from akcampaign.schemas.campaigns import (
    FeaturedCampaignsResponseSchema, CampaignResponseSchema, CampaignCashLoanResponseSchema, CampaignType, CampaignDetailsResponseSchema, CampaignDetailsGenericAdditionalFieldsResponseSchema,
    CampaignDetailsOnlineMerchantAdditionalFieldsResponseSchema,AdminCampaignResponseModelSchema,AdminCampaignDetailsResponseSchema
)


def get_campaigns_featured(campaign_data, campaign_credit_rating_data, campaign_investments_data, campaign_detail_data):
    d = dict()

    d.update(campaign_data)
    d.update(campaign_credit_rating_data)
    d.update(campaign_investments_data)
    d.update(campaign_detail_data)
    response = FeaturedCampaignsResponseSchema().dump(d)
    return response


def get_campaign(campaign_data, campaign_detail_data, campaign_investments_data, campaign_credit_rating, campaign_type):
    data = dict()
    data.update(campaign_credit_rating)
    data.update(campaign_data)
    data.update(campaign_detail_data)
    data.update(campaign_investments_data)
    if(campaign_type == "P2P_CASH_LOAN"):
        response = CampaignCashLoanResponseSchema().dump(data)
    
    else:
        response = CampaignResponseSchema().dump(data)
    return response

def get_final_campaign_details(
    campaign_data, campaign_detail_data, campaign_investments_data,campaign_credit_rating, campaign_partner, lang):

    data = dict()
    data.update(campaign_data)
    data.update(campaign_detail_data)
    data.update(campaign_investments_data)
    data.update(campaign_credit_rating)
    data.update(campaign_partner)
    data.update(lang)
    campaign_type = CampaignType[campaign_data["campaign_type"]]

    get_campaign_response_schema = CampaignDetailsResponseSchema()
    get_campaign_OM_fields_response_schema= CampaignDetailsOnlineMerchantAdditionalFieldsResponseSchema()
    get_campaign_generic_fields_response_schema = CampaignDetailsGenericAdditionalFieldsResponseSchema()

    schema_to_use = get_campaign_response_schema

    if campaign_type == CampaignType.P2P_ONLINE_MERCHANT:
        schema_to_use = get_campaign_OM_fields_response_schema

    elif campaign_type in [
        CampaignType.P2P_BUSINESS,
        CampaignType.P2P_INVOICE_FINANCING,
        CampaignType.P2P_INVENTORY_FINANCING,
        CampaignType.P2P_RECEIVEABLE_FINANCING,
        CampaignType.P2P_CAPEX_FINANCING,
        CampaignType.P2P_PORTFOLIO_FINANCING,
        CampaignType.P2P_INSTANT_FINANCING,
        CampaignType.P2P_DISTRIBUTOR_FINANCING,
        CampaignType.P2P_SUPPLY_CHAIN_FINANCING,
    ]:
        schema_to_use = get_campaign_generic_fields_response_schema

    response = schema_to_use.dump(data)

    return response



def get_campaign_admin(campaign_data, campaign_detail_data, campaign_investments_data, campaign_institutional_lender,campaign_credit_rating):
    data = dict()
    data.update(campaign_credit_rating)
    data.update(campaign_data)
    data.update(campaign_detail_data)
    data.update(campaign_investments_data)
    data.update(campaign_institutional_lender)

    response = AdminCampaignResponseModelSchema().dump(data)

    return response



def get_campaign_details_admin(campaign_data,campaign_detail_data,campaign_investments_data,campaign_institutional_lender,campaign_credit_rating,campaign_partner, campaign_payment_list):
    
    data = dict()
    data.update(campaign_data)
    data.update(campaign_detail_data)
    data.update(campaign_investments_data)
    data.update(campaign_payment_list)
    data.update(campaign_institutional_lender)
    data.update(campaign_credit_rating)
    data.update(campaign_partner)

    response = AdminCampaignDetailsResponseSchema().dump(data)

    return response