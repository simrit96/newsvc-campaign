from akcampaign.tools.gcp_logger import GCPLogging
from akcampaign.models.loans import LoanRequestStatus, LoanProductType, LoanProducts
from akcampaign.schemas.commons import CampaignStatus, CampaignType, TranslateCampaignStatus

from datetime import datetime


def translate_datetime_to_date(date_data):
    d = datetime.strptime(date_data, "%Y-%m-%dT%H:%M:%S")
    return d.date().strftime("%Y-%m-%d")

def translate_product_to_campaign_type(product_type: str):
    campaign_types = {
        LoanProductType.BUSINESS_LOAN.value: CampaignType.P2P_BUSINESS.name,
        LoanProductType.ONLINE_MERCHANT.value: CampaignType.P2P_ONLINE_MERCHANT.name,
        LoanProductType.LOAN_PURCHASE.value: CampaignType.P2P_LOAN_PURCHASE.name,
        LoanProductType.EMPLOYEE_LOAN.value: CampaignType.P2P_EMPLOYEE_LOAN.name,
        LoanProductType.PAYLATER_BUSINESS.value: CampaignType.P2P_INSTANT_FINANCING.name,
        LoanProductType.DISTRIBUTOR_FINANCING.value: (
            CampaignType.P2P_DISTRIBUTOR_FINANCING.name
        ),
        LoanProductType.SUPPLY_CHAIN_FINANCING.value: (
            CampaignType.P2P_SUPPLY_CHAIN_FINANCING.name
        ),
        LoanProductType.CASH_LOAN.value: CampaignType.P2P_CASH_LOAN.name,
    }

    return campaign_types.get(product_type)


def translate_status_to_core_campaign_status(
    loan_request_status: any, campaign_status: any):
    
    if loan_request_status == LoanRequestStatus.FUNDRAISING.name:
        statuses = {
            CampaignStatus.CLOSE.name: TranslateCampaignStatus.CLOSE_CAMPAIGN.name,
            CampaignStatus.OPEN.name: TranslateCampaignStatus.PRE_OPEN_CAMPAIGN.name,
            CampaignStatus.PRE_OPEN.name: TranslateCampaignStatus.PRE_OPEN_CAMPAIGN.name,
        }

        mapped_status = statuses.get(campaign_status)
    else:
        statuses = {
            LoanRequestStatus.PRE_DISBURSEMENT.name: (
                TranslateCampaignStatus.PRE_DISBURSEMENT.name
            ),
            LoanRequestStatus.WAITING_FOR_DOWN_PAYMENT.name: (
                TranslateCampaignStatus.WAITING_FOR_DOWN_PAYMENT.name
            ),
        }

        mapped_status = statuses.get(loan_request_status)

    if not mapped_status:
        return TranslateCampaignStatus.DRAFT.name

    return mapped_status


def translate_value_for_campaigns(key: str, value: any):
    if key == "product_id":
        product = LoanProducts.query.filter(LoanProducts.id == value,
        LoanProducts.is_deleted == False).first()
        value = translate_product_to_campaign_type(product.product_type)
    elif key == "loan_amount_approved":
        # min_funding is 80%, max_funding
        value = [0.8 * value, value]
    elif key in (
        "campaign_start_date",
        "campaign_end_date",
        "campaign_success_date",
    ):
        date = datetime.strptime(value, "%Y-%m-%dT%H:%M:%S.%f")
        value = date.strftime("%Y-%m-%d")
    elif key in (
        "active_transaction_since",
        "merchant_sales_period_from",
        "merchant_sales_period_to",
        "purchase_period_from",
        "purchase_period_to",
    ):
        date = datetime.strptime(value, "%Y-%m-%d")
        value = date.strftime("%Y-%m-%d")
    elif key in ["revenue_period_from", "revenue_period_to", "debt_capital_period_by"]:
        value = translate_datetime_to_date(value)
    elif key in (
        "campaign_launch_date",
        "campaign_fully_funded_date",
        "campaign_success_date",
    ):
        date = datetime.strptime(value, "%Y-%m-%dT%H:%M:%S.%f")
        value = date.strftime("%Y-%m-%d %H:%M:%S")

    if type(value) == list:
        return value
    else:
        return [value]


def translateLoanRequestToCampaign(loan_request_dict):
    try:
        payload = dict()

        required_field_dict = dict(
            user_id=("user_id",),
            industry_category_id=("category_id",),
            product_id=("campaign_type",),
            loan_request_status=("status",),
            campaign_name=("campaign_name",),
            uuid=("uuid",),
            id=("id",),
            credit_rating = ("credit_rating",),
            installment_length_type_approved=("installment_length_unit",),
            installment_length_approved=("installment_length",),
            principal_payment_freq=("installment_payment_freq",),
            interest_payment_freq=("interest_payment_freq",),
            is_insured=("has_insurance",),
            credit_rating_id=("loan_credit_rating_id",),
            minimum_lending_amount=("minimum_investment",),
            campaign_launch_date=("active_date",),
            pre_disbursement_trigger_point=("pre_disbursement_trigger_point",),
            loan_amount_approved=("min_funding", "max_funding"),
            campaign_success_date=("success_date",),
            campaign_banner_url=("cover",),
            campaign_start_date=("campaign_start",),
            approved_effective_interest=("effective_interest",),
            approved_flat_interest=("flat_interest",),
            campaign_status=("status",),
            institutional_lender_id=("institutional_lender_id",),
            campaign_end_date=("campaign_end",),
            is_featured=("is_featured",),
            highlight_type=("highlight_type",),
            loan_description=("description",),
            campaign_visibility=("campaign_visibility",),
            )
       
        for key, value in required_field_dict.items():
            loan_request_data = loan_request_dict.get(key)

            if loan_request_data:
                if key in ("loan_request_status", "campaign_status"):
                    if payload.get("status"):
                        continue

                    loan_request_status = (
                        None
                        if not loan_request_dict.get("loan_request_status")
                        else loan_request_dict.get("loan_request_status")
                    )
                    campaign_status = (
                        None
                        if not loan_request_dict.get("campaign_status")
                        else loan_request_dict.get("campaign_status")
                    )   

                    translated_value = [
                        translate_status_to_core_campaign_status(
                            loan_request_status, campaign_status
                        )
                    ]
                else:
                    translated_value = translate_value_for_campaigns(key, loan_request_data)
            else:
                continue

            for index, core_key in enumerate(value):
                payload[core_key] = translated_value[index]

        
        return payload

    except Exception as e:
        GCPLogging.error(e, "Failed to translate loan data to campaign data")
        return None



def LoanRequestDetailsToCampaignDetails(loan_request_details):
    try:

        required_fields = dict(
            active_transaction_since=("active_transaction_since",),
            form_of_business=("form_of_business",),
            transaction_period_duration=("transaction_period_duration",),
            total_transaction_amount=("total_transaction_amount",),
            loan_to_value=("loan_to_value",),
            address=("address",),
            establishment_year=("year_of_establishment",),
            revenue=("revenue",),
            total_equity=("capital",),
            loan_purpose_description=("loan_purpose",),
            ebitda=("ebitda",),
            short_term_liabilities=("short_term_debt",),
            long_term_liabilities=("long_term_debt",),
            debt_to_equity=("debt_equity_ratio",),
            debt_to_assets=("debt_asset_ratio",),
            debt_to_service_coverage=("debt_service_ratio",),
            merchant_sales_period_from=("sales_period_from",),
            merchant_sales_period_to=("sales_period_to",),
            merchant_sales_amount=("sales_amount",),
            merchant_sales_time_duration=("total_sales_time",),
            revenue_period_from=("revenue_period_from",),
            revenue_period_to=("revenue_period_to",),
            debt_capital_period_by=("debt_capital_period_by",),
            transaction_period_on_platform=("transaction_period_on_platform",),
            purchase_period_from=("purchase_period_from",),
            purchase_period_to=("purchase_period_to",),
            collateral_description=("guarantee_collateral_description",),
            partner_id=("partner_id",),
            business_description=("business_description",),
            collateral_type=("collateral_type",),
            id=("campaign_id",),
        )

        payload = dict()

        for key, value in required_fields.items():
            loan_request_data = loan_request_details.get(key)

            if loan_request_data:
                translated_value = translate_value_for_campaigns(key, loan_request_data)
            else:
                continue

            for index, core_key in enumerate(value):
                payload[core_key] = translated_value[index]

        return payload
    
    except Exception as e:
        GCPLogging.error(e, "Failed to translate loan request details to campaign details")
        return None