from akcampaign import db
from enum import Enum
from akcampaign.models.commons import BaseModelForLoans

class InstallmentLengthType(Enum):
    HARIAN = u"Harian"
    MINGGUAN = u"Mingguan"
    BULANAN = u"Bulanan"
    TIGA_BULANAN = u"3 Bulanan"
    ENAM_BULANAN = u"6 Bulanan"
    TAHUNAN = u"Tahunan"


class PrincipalPaymentFreq(Enum):
    MONTHLY = u"Monthly"
    BULLET = u"Bullet"
    WEEKLY = u"Weekly"


class InterestPaymentFreq(Enum):
    MONTHLY = u"Monthly"
    BULLET = u"Bullet"
    WEEKLY = u"Weekly"


class CreditRatingType(Enum):
    CREDIT_RATING_BUSINESS = u"Credit Rating Business"
    CREDIT_RATING_EMPLOYEE_LOAN = u"Credit Rating Employee Loan"
    CREDIT_RATING_PEFINDO = u"Credit Rating Pefindo"

class LoanProductType(Enum):
    BUSINESS_LOAN = "Business Loan"
    SUPPLY_CHAIN_FINANCING = "Supply Chain Financing"
    ONLINE_MERCHANT = "Online Merchant Financing"
    DISTRIBUTOR_FINANCING = "Distributor/Buyer Financing"
    EMPLOYEE_LOAN = "Employee Loan"
    PAYLATER_BUSINESS = "PayLater Business"
    LOAN_PURCHASE = "Loan Purchase Financing"
    INSTANT_FINANCING = "Instant Financing"
    CASH_LOAN = "Cash Loan"

class CollateralType(Enum):
    INVOICE = u"invoice"
    PRE_INVOICE = u"pre_invoice"
    INVENTORY = u"inventory"
    CAPEX = u"capex"
    FIXED_ASSET = u"fixed_asset"
    NON_COLLATERAL = u"non collateral"
    SALARY_SLIP = u"salary slip"

class CampaignStatus(Enum):
    DRAFT = u"Draft"
    CLOSE = u"Close"
    PRE_OPEN = u"Pre Open"
    OPEN = u"Open"
    FINAL_VERIFICATION = u"Final Verification"
    CAMPAIGN_SUCCESS = u"Campaign Success"


class LoanRequestStatus(Enum):
    DRAFT = u"Draft"
    UNDER_REVIEW = u"Under Review"
    FUNDRAISING = u"Fundraising"
    FUNDRAISING_FAILED = u"Fundraising Failed"
    FINAL_VERIFICATION = u"Final Verification" 
    DISBURSEMENT_ON_PROCESS = u"Disbursement On Process"
    DISBURSEMENT_SUCCESS = u"Disbursement Success"
    DISBURSEMENT_FAILED = u"Disbursement Failed"
    REJECTED_BY_AKSELERAN = u"Rejected by Akseleran"
    REJECTED_BY_BORROWER = u"Rejected by Borrower"
    REJECTED_BY_PARTNER = u"Rejected by Partner"
    WAITING_PARTNER_CONFIRMATION = u"Waiting Partner Confirmation"
    WAITING_FOR_FUNDRAISING = u"Waiting For Fund Raising"
    WAITING_FOR_SIGNATURE = u"Waiting for Signature"
    WAITING_FOR_BORROWER_APPROVAL = u"Waiting For Borrower Approval"
    DONE = u"Done"
    PRE_DISBURSEMENT = u"Pre Disbursement"
    READY_TO_DISBURSE = u"Ready to Disburse"
    WAITING_FOR_DOWN_PAYMENT = u"Waiting for down payment"
    WAITING_FOR_FDC = u"Waiting For FDC"


class CampaignVisibilityType(Enum):
    NOT_SELECTED = u"Not Selected"
    CLOSE = u"Close"
    OPEN = u"Open"


class LoanPurposeType(Enum):
    BUSINESS_LOAN = u"Pinjaman modal usaha"
    EDUCATION = u"Pendidikan"
    DAILY_NEEDS = u"Kebutuhan sehari hari"
    CONSUMPTIVE_NEEDS = u"Keperluan konsumtif"
    MEDICAL_EXPENSES = u"Pengobatan atau kesehatan"
    VACATION = u"Liburan"
    MATERNITY = u"Melahirkan"
    WEDDING = u"Pernikahan"
    RENOVATION = u"Renovasi rumah"
    OTHERS = u"Lainnya"


class LoanRequests(BaseModelForLoans):
    __bind_key__ = "db2"
    __tablename__ = "loan_requests"


    uuid = db.Column(db.String(500), unique=True)
    user_id = db.Column(db.Integer(), nullable=False)
    institutional_lender_id = db.Column(db.Integer())
    
    loan_request_status = db.Column(
        db.Enum(LoanRequestStatus, name="loan_request_status"),
        default=LoanRequestStatus.DRAFT,
        server_default=f"{LoanRequestStatus.DRAFT.name}",
    )
    campaign_status = db.Column(
        db.Enum(CampaignStatus, name="campaign_status"),
        default=CampaignStatus.DRAFT,
        server_default=f"{CampaignStatus.DRAFT.name}",
    )
    product_id = db.Column(db.Integer(), db.ForeignKey("loan_products.id"))
    collateral_type = db.Column(
        db.Enum(CollateralType, name="collateral_type"), nullable=True
    )
    collateral_summary = db.Column(db.Text())
    collateral_description = db.Column(db.Text())
    loan_request_number = db.Column(db.String(500))
    loan_amount_requested = db.Column(db.Float(), server_default="0")
    loan_amount_approved = db.Column(db.Float(), server_default="0")
    installment_length_requested = db.Column(db.Integer())
    installment_length_approved = db.Column(db.Integer())
    installment_length_type_requested = db.Column(
        db.Enum(InstallmentLengthType, name="installment_length_type"),
        default=InstallmentLengthType.BULANAN,
        server_default=f"{InstallmentLengthType.BULANAN.name}",
    )
    installment_length_type_approved = db.Column(
        db.Enum(InstallmentLengthType, name="installment_length_type"),
        default=InstallmentLengthType.BULANAN,
        server_default=f"{InstallmentLengthType.BULANAN.name}",
    )
    principal_payment_freq = db.Column(
        db.Enum(PrincipalPaymentFreq, name="principal_payment_freq"),
        default=PrincipalPaymentFreq.MONTHLY,
        server_default=f"{PrincipalPaymentFreq.MONTHLY.name}",
    )
    interest_payment_freq = db.Column(
        db.Enum(InterestPaymentFreq, name="interest_payment_freq"),
    )
    effective_interest = db.Column(db.Float())
    flat_interest = db.Column(db.Float())    
    is_insured = db.Column(db.Boolean())
    minimum_lending_amount = db.Column(db.Float(), default=100000)
    campaign_start_date = db.Column(db.DateTime())
    campaign_end_date = db.Column(db.DateTime())
    campaign_launch_date = db.Column(db.DateTime())
    campaign_fully_funded_date = db.Column(db.DateTime())
    campaign_success_date = db.Column(db.DateTime())
    loan_purpose_description = db.Column(db.Text())
    business_description = db.Column(db.Text())
    partner_id = db.Column(db.Integer())
    campaign_name = db.Column(db.String(500))
    campaign_banner_url = db.Column(db.String(500))
    loan_description = db.Column(db.Text)
    credit_rating_id = db.Column(db.Integer())
    is_featured = db.Column(db.Boolean(), server_default="false")
    pre_disbursement_trigger_point = db.Column(
        db.Float(), default=0.6, server_default="0.6"
    )

    campaign_visibility = db.Column(
        db.Enum(CampaignVisibilityType, name="campaign_visibility_type"),
        default=CampaignVisibilityType.NOT_SELECTED,
        server_default=f"{CampaignVisibilityType.NOT_SELECTED.name}",
        nullable=False,
    )
    loan_purpose = db.Column(
        db.Enum(LoanPurposeType, name="loan_purpose_type"),
        default=LoanPurposeType.OTHERS,
        server_default=f"{LoanPurposeType.OTHERS.name}",
        nullable=False)

    details = db.relationship("LoanRequestDetail", primaryjoin=(
            "and_(LoanRequests.id==LoanRequestDetail.loan_request_id," "LoanRequestDetail.is_deleted==False)"), uselist=False)



class LoanRequestDetail(BaseModelForLoans):
    __bind_key__ = "db2"
    __tablename__ = "loan_request_details"
    loan_request_id = db.Column(db.Integer(), db.ForeignKey("loan_requests.id"), nullable=False)
    revenue = db.Column(db.Float(), default=0)
    total_equity = db.Column(db.Float(), default=0)
    ebitda = db.Column(db.Float(), default=0)
    establishment_year = db.Column(db.Integer())
    net_profit = db.Column(db.Float(), default=0)
    net_income = db.Column(db.Float(), default=0)
    short_term_liabilities = db.Column(db.Float(), default=0)
    long_term_liabilities = db.Column(db.Float(), default=0)
    capital = db.Column(db.Float(), default=0)
    debt_to_equity = db.Column(db.Float(), default=0)
    debt_to_assets = db.Column(db.Float(), default=0)
    debt_to_service_coverage = db.Column(db.Float(), default=0)
    revenue_period_from = db.Column(db.DateTime())
    revenue_period_to = db.Column(db.DateTime())
    debt_capital_period_by = db.Column(db.DateTime())
    industry_category_id = db.Column(db.Integer())
    form_of_business = db.Column(db.String())
    address = db.Column(db.String())

    # OMF
    merchant_platform_reputation = db.Column(db.String(150))
    merchant_sales_period_from = db.Column(db.Date())
    merchant_sales_period_to = db.Column(db.Date())
    merchant_sales_amount = db.Column(db.Float(), default=0)
    merchant_sales_time_duration = db.Column(db.Float())

    # DF/IF
    active_transaction_since = db.Column(db.Date())
    purchase_period_from = db.Column(db.Date())
    purchase_period_to = db.Column(db.Date())
    partner_notes = db.Column(db.Text())
    transaction_period_duration = db.Column(db.Integer)
    transaction_period_on_platform = db.Column(db.String(50))
    total_transaction_amount = db.Column(db.Float())


class LoanProducts(BaseModelForLoans):
    __bind_key__ = "db2"
    __tablename__ = "loan_products"
    product_name = db.Column(db.String(500), nullable=False)
    product_type = db.Column(db.String(500), nullable=False)
    product_code = db.Column(db.String(5), nullable=False)
    min_loan_amount = db.Column(db.BigInteger, default=0, nullable=False)
    max_loan_amount = db.Column(db.BigInteger, default=0, nullable=False)
    is_active = db.Column(
        db.Boolean(), default=False, nullable=True, server_default="false"
    )
    pre_disbursement_trigger_point = db.Column(
        db.Float(), default=0.6, server_default="0.6"
    )

class LoanCreditRatings(BaseModelForLoans):
    __bind_key__ = "db2"
    __tablename__ = "credit_ratings"

    credit_rating = db.Column(db.String())
    is_active = db.Column(db.Boolean(), default = True)
    credit_rating_type = db.Column(db.Enum(CreditRatingType, name = "credit_rating_type"))