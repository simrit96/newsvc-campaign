import sqlalchemy as sa
from akcampaign import db

from datetime import datetime


from sqlalchemy.ext.compiler import compiles
from sqlalchemy.sql import expression
from sqlalchemy.types import DateTime


class BaseModel(db.Model):
    __abstract__ = True

    id = sa.Column(sa.Integer(), primary_key=True)
    created = sa.Column(
        sa.DateTime(),
        default=sa.func.now(),
        server_default=sa.func.now(),
        nullable=False,
    )
    modified = sa.Column(
        sa.DateTime(),
        default=sa.func.now(),
        onupdate=sa.func.now(),
        nullable=False,
        server_default=sa.func.now(),
        server_onupdate=sa.func.now(),
    )
    is_deleted = sa.Column(sa.Boolean(), default=False, server_default="false")
    deleted = sa.Column(sa.DateTime(), default=None)

    def date_fmt(date):
        return date.strftime("%Y-%m-%d %H:%M:%S")



class utcnow(expression.FunctionElement):
    type = DateTime()


@compiles(utcnow, "postgresql")
def pg_utcnow(element, compiler, **kw):
    return "TIMEZONE('utc', CURRENT_TIMESTAMP)"


class BaseModelForLoans(db.Model):  # pragma: no cover
    """
    This class is the abstract model that inherited from other model
    """

    __abstract__ = True

    id = sa.Column(sa.Integer(), primary_key=True)
    created_at = sa.Column(
        sa.DateTime(),
        default=datetime.utcnow,
        server_default=utcnow(),
        nullable=False,
    )
    updated_at = sa.Column(
        sa.DateTime(),
        default=datetime.utcnow,
        onupdate=utcnow(),
        nullable=False,
        server_default=utcnow(),
        server_onupdate=utcnow(),
    )
    is_deleted = sa.Column(sa.Boolean(), default=False, server_default="false")
    deleted_at = sa.Column(sa.DateTime(), default=None)


    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return self
        
        except Exception:
            db.session.rollback()
            raise