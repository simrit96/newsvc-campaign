from datetime import datetime
from akcampaign import ma
from enum import Enum
from akcampaign.tools.localization import Localization


class TranslateCampaignStatus(Enum):
    DRAFT = u"Draft"
    CLOSE_CAMPAIGN = u"Close Campaign"
    OPEN_CAMPAIGN = u"Open Campaign"
    FINAL_VERIFICATION = u"Final Verification"
    CAMPAIGN_SUCCESS = u"Campaign Success"
    PRE_OPEN_CAMPAIGN = u"Pre Open Campaign"
    TAHAP_PENCAIRAN = u"Tahap Pencairan"
    UNDER_REVIEW = u"Under Review"
    REJECTED_BY_USER = u"Rejected by user"
    PRE_DISBURSEMENT = u"Pre Disbursement"
    WAITING_FOR_DOWN_PAYMENT = u"Waiting for down payment"


class CampaignStatus(Enum):
    DRAFT = u"Draft"
    CLOSE = u"Close"
    PRE_OPEN = u"Pre Open"
    OPEN = u"Open"
    FINAL_VERIFICATION = u"Final Verification"
    CAMPAIGN_SUCCESS = u"Campaign Success"


class CollateralType(Enum):
    INVOICE = "Invoice"
    SPK_PO_KONTRAK = "SPK/PO/Kontrak"
    INVENTORY = "Inventory"
    OTHER_FIXED_ASSET = "Other Fixed Asset (Tanah, Bangunan, Machinery, dll)"
    CAPEX = "Capex"
    FIXED_ASSET = "Fixed Asset"
    NON_COLLATERAL = "Non Collateral"
    SALARY_SLIP = "Salary Slip"

    def get_collateral_type(obj):
        for i in CollateralType:
            if obj == i.name:
                return i.value


class CampaignType(Enum):
    P2P_BUSINESS = "P2P Lending - Business Loan"
    P2P_ONLINE_MERCHANT = "P2P Lending - Online Merchant"
    P2P_LOAN_PURCHASE = "P2P Lending - Loan Purchase"
    P2P_EMPLOYEE_LOAN = "P2P Lending - Employee Loan"
    EQUITY_CROWDFUNDING = "Equity Crowdfunding"
    P2P_INVOICE_FINANCING = "P2P Lending - Invoice Financing"
    P2P_RECEIVEABLE_FINANCING = "P2P Lending - Receivable Financing"
    P2P_CAPEX_FINANCING = "P2P Lending - CAPEX Financing"
    P2P_INVENTORY_FINANCING = "P2P Lending - Inventory Financing"
    P2P_PORTFOLIO_FINANCING = "P2P Lending - Portfolio Financing"
    P2P_INSTANT_FINANCING = "P2P Lending - Instant Financing"
    P2P_DISTRIBUTOR_FINANCING = "P2P Lending - Distributor Financing"
    P2P_SUPPLY_CHAIN_FINANCING = "P2P Lending - Supply Chain Financing"
    P2P_CASH_LOAN = "P2P Lending - Cash Loan"


    def get_campaign_type(obj):
        for i in CampaignType:
            if obj == i.name:
                return i.value


class InstallmentLengthUnit(Enum):
    HARI = "Hari"
    BULAN = "Bulan"

    def get_installment_length_unit(obj):
        for i in InstallmentLengthUnit:
            if obj == i.name:
                return i.value

class HighlightType(Enum):
    FINANCIAL = "financial"
    BORROWER = "borrower"
    DISTRIBUTOR = "distributor"
    ONLINE_MERCHANT = "online-merchant"

    def get_highlight_type(obj):
        for i in HighlightType:
            if obj == i.name:
                return i.value
        

class InstallmentPaymentFreq(Enum):
    MONTHLY = "Bulanan"
    WEEKLY = "Mingguan"
    TRIMONTHLY = "Triwulanan"
    QUADMONTHLY = "Empat Bulanan"
    SIXMONTHLY = "Enam Bulanan"
    ANNUALLY = "Tahunan"
    BULLET = "Bullet Payment"

    def get_installment_payment_freq(obj):
        for i in InstallmentPaymentFreq:
            if obj == i.name:
                return i.value

class InterestPaymentFreq(Enum):
    MONTHLY = "Bulanan"
    WEEKLY = "Mingguan"

    def get_interest_payment_freq(obj):
        for i in InterestPaymentFreq:
            if obj == i.name:
                return i.value


class BaseSchema(ma.SQLAlchemyAutoSchema):

    def get_campaign_start(self, obj):
        try:
            start_date = obj['campaign_start'].split(".")

            date = datetime.strptime(start_date[0], "%Y-%m-%dT%H:%M:%S")

            return date.strftime("%Y-%m-%d %H:%M:%S")

        except Exception as e:
            return None

    def get_campaign_end(self, obj):
        try:
            end_date = obj['campaign_end'].split(".")
            date = datetime.strptime(end_date[0], "%Y-%m-%dT%H:%M:%S")
            return date.strftime("%Y-%m-%d %H:%M:%S")

        except Exception:
            return None

    def get_campaign_time_remaining(self, obj):
        try:
            if obj["funded_percentage"] == 100:
                return "Berakhir"

            end_date = obj["campaign_end"].split(".")

            hours_remaining = (
                datetime.strptime(end_date[0], "%Y-%m-%dT%H:%M:%S") - datetime.now()
            ).total_seconds() / 60

            if hours_remaining < 0:
                return "Berakhir"

            if hours_remaining > 24:
                return (
                    str(
                        (
                            datetime.strptime(end_date[0], "%Y-%m-%dT%H:%M:%S")
                            - datetime.now()
                        ).days
                    )
                    + " Hari lagi"
                )

            return str(hours_remaining) + " Jam lagi"
        except Exception:
            return None

    def get_rounded_effective_interest(self, obj):
        try:
            return round(obj["effective_interest"], 1)
        except Exception:
            return None

    def get_installment_length_unit(self, obj):
        try:
            response = {
                "name": obj["installment_length_unit"],
                "value": InstallmentLengthUnit.get_installment_length_unit(
                    obj["installment_length_unit"]
                ),
            }

            if "lang" in obj and obj["lang"] == "en":
                response["name"] = Localization.translate(response["name"], "en")
                response["value"] = Localization.translate(response["value"], "en")

            return response
        except Exception as e:
            return None

    def get_installment_payment_freq(self, obj):
        try:
            response = {
                "name": obj["installment_payment_freq"],
                "value": InstallmentPaymentFreq.get_installment_payment_freq(
                    obj["installment_payment_freq"]
                ),
                "text": InstallmentPaymentFreq.get_installment_payment_freq(
                    obj["installment_payment_freq"]
                ),
            }

            if "lang" in obj and obj["lang"] == "en":
                response["name"] = Localization.translate(response["name"], "en")
                response["value"] = Localization.translate(response["value"], "en")
                response["text"] = Localization.translate(response["text"], "en")

            return response
        except Exception:
            return None

    def get_interest_payment_freq(self, obj):
        try:
            response = {
                "name": obj["interest_payment_freq"],
                "value": InterestPaymentFreq.get_interest_payment_freq(
                    obj["interest_payment_freq"]
                ),
            }

            if "lang" in obj and obj["lang"] == "en":
                response["name"] = Localization.translate(response["name"], "en")
                response["value"] = Localization.translate(response["value"], "en")

            return response
        except Exception:
            return None

    def get_highlight_type(self, obj):
        try:
            if obj["highlight_type"] is None:
                return None

            response = {
                "name": obj["highlight_type"],
                "value": HighlightType.get_highlight_type(obj["highlight_type"]),
            }

            return response
        except Exception:
            return None

    def get_campaign_name(self, obj):
        try:
            if "lang" in obj and obj["lang"] == "en":
                campaign_name = obj["campaign_name"].split(" ")
                campaign_name[0] = Localization.translate(campaign_name[0], "en")
                return " ".join(campaign_name)

            return obj["campaign_name"]
        except Exception:
            return None

    
    def get_campaign_type(self, obj):
        try:
            response = {
                "name": obj["campaign_type"],
                "value": CampaignType.get_campaign_type(obj["campaign_type"]),
            }

            return response
        except Exception:
            return None

    def get_collateral_type(self, obj):
        try:
            return {
                "name": obj["collateral_type"],
                "value": CollateralType.get_collateral_type(obj["collateral_type"]),
            }
        except Exception:
            return None

    def get_invoice(self, obj):
        return {"is_attach": False}


    def get_active_date(self, obj):
        try:
            start_date = obj['active_date'].split(".")

            date = datetime.strptime(start_date[0], "%Y-%m-%dT%H:%M:%S")

            return date.strftime("%Y-%m-%d %H:%M:%S")

        except Exception as e:
            return None