from akcampaign import ma
from marshmallow import fields

from akcampaign.models.loans import LoanRequests, LoanRequestDetail, LoanCreditRatings
from akcampaign.tools.gcp_logger import GCPLogging
from slugify import slugify


class LoanRequestSchema(ma.SQLAlchemyAutoSchema):
    
    def getLoanRequests(self, **kwargs):
        try:
            data = {}
            loan_request_query = LoanRequests.query.filter(LoanRequests.is_deleted == False)
            loan_request_query = loan_request_query.filter(LoanRequests.uuid == kwargs.get('uuid')).first()
            data['loan_request'] = loan_request_query

            return data
        except Exception as e:
            GCPLogging.error(e, "Failed to get Loan Request Details")
            return None


class LoanCreditRatingSchema(ma.SQLAlchemyAutoSchema):

    class Meta:
        model = LoanCreditRatings

    credit_rating = fields.String()
    
    def get_credit_rating(self, credit_rating_id):
        try:
            loan_credit_ratings_query = LoanCreditRatings.query.filter(LoanCreditRatings.is_deleted == False)
            loan_credit_ratings_query = loan_credit_ratings_query.filter(LoanCreditRatings.id == credit_rating_id).first()

            res = {}
            res['credit_rating'] = loan_credit_ratings_query
            return res

        except Exception as e:
            GCPLogging.error(e, "Failed to get credit rating value in loan requests")
            return None

class LoanResponseSchema(ma.SQLAlchemyAutoSchema):

    class Meta:
        model = LoanRequests

    product_id = fields.Integer()
    interest_payment_freq = fields.Method("get_interest_payment_freq")
    campaign_status = fields.Method("get_campaign_type")
    credit_rating_id = fields.Integer()
    loan_request_status = fields.Method("get_loan_request_status")
    installment_length_type_requested = fields.Method("get_installment_length_type_requested")
    loan_purpose = fields.Method("get_loan_purpose")
    campaign_visibility = fields.Method("get_campaign_visibility")
    principal_payment_freq = fields.Method("get_principal_payment_freq")
    collateral_type = fields.Method("get_collateral_type")
    slug = fields.Method('slug')
    installment_length_type_approved = fields.Method("get_installment_length_type_approved")

    def slug(self, obj):
        try:
            return slugify(obj.campaign_name)
        except Exception:
            return None

    def get_interest_payment_freq(self, obj):
        try:
            return obj.interest_payment_freq.value

        except Exception:
            return None

    def get_campaign_type(self, obj):
        try:
            return obj.campaign_status.value
        except Exception:
            return None

    def get_loan_request_status(self, obj):
        try:
            return obj.loan_request_status.value
        except Exception:
            return None

    def get_installment_length_type_requested(self, obj):
        try:
            return obj.installment_length_type_requested.value
        except Exception:
            return None

    def get_installment_length_type_approved(self, obj):
        try:
            return obj.installment_length_type_approved.value
        except Exception:
            return None

    def get_loan_purpose(self,obj):
        try:
            return obj.loan_purpose.value
        except Exception:
            return None

    def get_campaign_visibility(self, obj):
        try:
            return obj.campaign_visibility.value
        except Exception:
            return None

    def get_collateral_type(self, obj):
        try:
            return obj.collateral_type.value
        except Exception:
            return None

    def get_principal_payment_freq(self, obj):
        try:
            return obj.principal_payment_freq.value
        except Exception:
            return None

    

class LoanDetailsResponseSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = LoanRequestDetail