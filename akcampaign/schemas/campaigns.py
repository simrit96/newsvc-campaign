from akcampaign.models.campaigns import Campaign, CampaignCreditRatings, CampaignInvestments, CampaignCategories, CampaignDetails, CampaignInstitutionalLender,CampaignPartner, CampaignPaymentList
from akcampaign import ma
from marshmallow import fields
from enum import Enum
from datetime import datetime

from akcampaign.schemas.commons import BaseSchema, CampaignType
from akcampaign.tools.gcp_logger import GCPLogging

class CampaignStatus(Enum):
    DRAFT = "Draft"
    UNDER_REVIEW = "Under Review"
    APPROVED_BY_USER = "Approved by User"
    REJECTED_BY_USER = "Rejected by User"
    CLOSE_CAMPAIGN = "Close Campaign"
    OPEN_CAMPAIGN = "Open Campaign"
    PROPOSAL_REJECTED = "Proposal Rejected"
    FINAL_VERIFICATION = "Final Verification"
    FINAL_VERIFICATION_FAILED = "Final Verification Failed"
    TARGET_FAIL_TO_ACHIEVED = "Target Fail to Achieved"
    CAMPAIGN_SUCCESS = "Campaign Success"
    PRE_OPEN_CAMPAIGN = "Pre-Open Campaign"
    TAHAP_PENCAIRAN = "Tahap Pencairan"
    LOCKED = "Locked"
    PRE_DISBURSEMENT = "Pre Disbursement"
    READY_TO_DISBURSE = "Ready to Disburse"
    DONE = "Done"
    WAITING_FOR_DOWNPAYMENT = "Waiting for Downpayment"

    def get_status(obj):
        for i in CampaignStatus:
            if i.name == obj:
                return i.value

class CampaignDetailsSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = CampaignDetails
        
    partner_id = ma.auto_field(dump_only = True)
    platform_id = ma.auto_field(dump_only = True)
    merchant_reputation_id = ma.auto_field(dump_only = True)
   

class CampaignInstitutionalLenderSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = CampaignInstitutionalLender

    def get_institutional_lender(self, campaign_id):
        data = {}
        data["institutional_lender_id"] = 0

        campaign_institutional_lender_query = CampaignInstitutionalLender.query.filter(CampaignInstitutionalLender.is_deleted == False)

        campaign_institutional_lender_query = (campaign_institutional_lender_query.filter(CampaignInstitutionalLender.campaign_id == campaign_id).first())

        if campaign_institutional_lender_query is not None:
            data["institutional_lender_id"] = campaign_institutional_lender_query.institutional_lender_id
        else:
            data["institutional_lender_id"] = None

        return data


class CampaignSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Campaign

class CampaignCreditRatingsSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = CampaignCreditRatings    

    credit_rating = fields.String(data_key = 'loan_credit_rating')

    def get_credit_rating(self, loan_credit_rating_id):
        try:
            data = {}
            campaign_credit_rating_query = CampaignCreditRatings.query.filter(CampaignCreditRatings.id == loan_credit_rating_id).first()
            data["credit_rating"] = campaign_credit_rating_query
            return data

        except Exception as e:
            GCPLogging.error(e, "Exception occured while trying to get credit ratings")
            return None
        


class CampaignCreditRatingResponseSchema(ma.SQLAlchemySchema):
    class Meta:
        ordered = True
        model = CampaignCreditRatings

    id = ma.auto_field()
    credit_rating = fields.String(data_key = "value")

class CampaignInvestmentsSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = CampaignInvestments

class CampaignCategoriesResponseSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        ordered = True
        load_only = ['created', 'deleted', 'is_active', 'modified']
        model = CampaignCategories
    
    id = ma.auto_field()
    category_name = ma.auto_field()
    parent_id = ma.auto_field()

class CampaignCategoriesRequestSchema(ma.SQLAlchemyAutoSchema):
    
    def get_categories(self):
        try:
            res = {}
            get_categories_query = CampaignCategories.query.filter(CampaignCategories.is_active == True)
            res["campaign_categories"] = get_categories_query.all()
            return res

        except Exception as e:
            GCPLogging.error(e, "Error occured while trying to get campaign categories")
            return res

    
class FeaturedCampaignsRequestSchema(ma.SQLAlchemyAutoSchema):

    def get_featured_campaigns(self, params):

        allowed_featured_campaign_status = [
            CampaignStatus.DRAFT.name,
            CampaignStatus.UNDER_REVIEW.name,
            CampaignStatus.APPROVED_BY_USER.name,
            CampaignStatus.OPEN_CAMPAIGN.name,
            CampaignStatus.FINAL_VERIFICATION.name,
            CampaignStatus.CAMPAIGN_SUCCESS.name,
            ]

        allowed_featured_campaign_types = [
            CampaignType.P2P_BUSINESS.name,
            CampaignType.P2P_ONLINE_MERCHANT.name,
            CampaignType.P2P_LOAN_PURCHASE.name,
            CampaignType.P2P_EMPLOYEE_LOAN.name,
            CampaignType.P2P_CAPEX_FINANCING.name,
            CampaignType.P2P_INVENTORY_FINANCING.name,
            CampaignType.P2P_INVOICE_FINANCING.name,
            CampaignType.P2P_PORTFOLIO_FINANCING.name,
            CampaignType.P2P_RECEIVEABLE_FINANCING.name,
            CampaignType.P2P_INSTANT_FINANCING.name,
            CampaignType.P2P_DISTRIBUTOR_FINANCING.name,
            CampaignType.P2P_SUPPLY_CHAIN_FINANCING.name,
        ]

        try:
            data = {}
            campaign_featured_query = Campaign.query.filter(Campaign.is_deleted.is_(False))
            featured_campaigns_query = campaign_featured_query.filter(Campaign.is_featured.is_(True), Campaign.campaign_visibility == "OPEN", Campaign.campaign_type.in_(allowed_featured_campaign_types),
            Campaign.status.in_(allowed_featured_campaign_status)).order_by(Campaign.active_date.desc())

            if(params.get('limit') is not None):
                featured_campaigns_query = featured_campaigns_query.limit(params.get('limit'))
            else:
                featured_campaigns_query = featured_campaigns_query.limit(6)

            data['featured_campaigns'] = featured_campaigns_query.all()
            return data

        except Exception as e:
            GCPLogging.error(e, 'Exception when trying to get featured campaigns')
            return None


class FeaturedCampaignsResponseSchema(BaseSchema):
    class Meta:
        ordered = True

    id = fields.String(data_key = 'campaign_id')
    loan_credit_rating = fields.String()
    campaign_name = fields.String()
    business_description = fields.String(data_key = 'description')
    max_funding = fields.Float()
    installment_length = fields.Integer()
    installment_length_unit = fields.Method('get_installment_length_unit')
    flat_interest = fields.Float()
    effective_interest = fields.Float()
    slug = fields.String()
    rounded_effective_interest = fields.Method("get_rounded_effective_interest")
    installment_payment_freq = fields.Method("get_installment_payment_freq")
    interest_payment_freq = fields.Method("get_interest_payment_freq")
    uuid = fields.String()
    funded_percentage = fields.Float()
    campaign_start = fields.Method("get_campaign_start")
    campaign_end = fields.Method("get_campaign_end")
    campaign_time_remaining = fields.Method("get_campaign_time_remaining")
    have_collateral = fields.Boolean()
    cover = fields.String()
    is_comingsoon = fields.Boolean()
    total_investors = fields.Integer()


class CampaignListRequestSchema(ma.SQLAlchemyAutoSchema):

    def get_all_campaigns(self, params):
        campaign_query = Campaign.query.filter(Campaign.is_deleted == False)

        campaign_query = campaign_query.join(Campaign.campaign_detail)
        try:
            if(params.get('campaign_visibility')):
                campaign_query = campaign_query.filter(Campaign.campaign_visibility == params.get('campaign_visibility'))
            
        except Exception as e:
            pass
        
        try:
            if(params.get('campaign_status')):
                status_list = params.get('campaign_status').split(',')
                campaign_query = campaign_query.filter(Campaign.status.in_(status_list))
            
        except Exception as e:
            pass

        try:
            if(params.get('campaign_type')):
                type_list = params.get('campaign_type').split(',')
                campaign_query = campaign_query.filter(Campaign.campaign_type.in_(type_list))
            
        except Exception as e:
            pass

        try:
            if params.get('category_ids'):
                categories = params.get('category_ids').split(",")
                categories_int = []
                for category in categories:
                    value = int(category)
                    categories_int.append(value)
                campaign_query = campaign_query.filter(
                    Campaign.category_id.in_(categories_int))
        except Exception as e:
            pass

        try:
            if(params.get('have_collateral')):
                campaign_query = campaign_query.filter(CampaignDetails.have_collateral == params.get('have_collateral'))
            
        except Exception as e:
            pass

        try:
            if(params.get('min_installment_range') and params.get('max_installment_range')):
                campaign_query = campaign_query.filter(Campaign.installment_length > params.get('min_installment_range'), Campaign.installment_length <= params.get('max_installment_range'))
            
        except Exception as e:
            pass

        try:
            if(params.get('min_interest_range') and params.get('max_interest_range')):
                campaign_query = campaign_query.filter(Campaign.effective_interest > float(params.get('min_interest_range')), Campaign.effective_interest <= float(params.get('max_interest_range'))) 
            
        except Exception as e:
            pass

        try:

            if(params.get('location')):
                campaign_query = campaign_query.order_by(Campaign.status)

            if(params.get('sort')):
                passed_sort = params.get('sort').lower().split('_')
                if(passed_sort[0] == 'campaigndate'):
                    if(passed_sort[1] == 'asc'):
                        campaign_query = campaign_query.order_by(Campaign.active_date.asc())
                    else:
                        campaign_query = campaign_query.order_by(Campaign.active_date.desc())


                elif(passed_sort[0] == 'equity'):
                    if(passed_sort[1] == 'asc'):
                        campaign_query = campaign_query.order_by(Campaign.offered_equity.asc())
                    else:
                        campaign_query = campaign_query.order_by(Campaign.offered_equity.desc())

                elif(passed_sort[0] == 'capital'):
                    if(passed_sort[1] == 'asc'):
                        campaign_query = campaign_query.order_by(Campaign.min_funding.asc())
                    else:
                        campaign_query = campaign_query.order_by(Campaign.min_funding.desc())

                elif(passed_sort[0] == 'loan'):
                    if(passed_sort[1] == 'asc'):
                        campaign_query = campaign_query.order_by(Campaign.max_funding.asc())
                    else:
                        campaign_query = campaign_query.order_by(Campaign.max_funding.desc())

                elif(passed_sort[0] == 'interest'):
                    if(passed_sort[1] == 'asc'):
                        campaign_query = campaign_query.order_by(Campaign.flat_interest.asc())
                    else:
                        campaign_query = campaign_query.order_by(Campaign.flat_interest.desc())

            else:
                campaign_query = campaign_query.order_by(Campaign.created.desc())

        except Exception:
            campaign_query = campaign_query.order_by(Campaign.created.desc())
        

        
        data = {}
        data['count'] = campaign_query.count()

        try:
            if(params.get('offset')):
                campaign_query = campaign_query.offset(params.get('offset'))

        except Exception as e:
            pass


        try:
            if(params.get('limit')):
                campaign_query = campaign_query.limit(params.get('limit'))
            else:
                campaign_query = campaign_query.limit(6)
        except Exception as e:
            pass
        data['campaigns'] = campaign_query
        return data


class CampaignCashLoanResponseSchema(BaseSchema):
    class Meta:
        ordered = True

    campaign_name = fields.String()
    campaign_start = fields.Method("get_campaign_start")
    campaign_end = fields.Method("get_campaign_end")
    campaign_time_remaining = fields.Method("get_campaign_time_remaining")
    id = fields.Integer()
    uuid = fields.String()
    slug = fields.String()
    campaign_type = fields.String()
    category_id = fields.Integer()
    min_funding = fields.String()
    max_funding = fields.String()
    logo = fields.String()
    cover = fields.String()
    status = fields.String()
    total_investment = fields.Float()
    total_investors = fields.Integer()
    funded_percentage = fields.Float()
    campaign_creator_name_anonymous = fields.String()
    institutional_lender_id = fields.Integer()
    has_insurance = fields.Boolean()


class CampaignResponseSchema(CampaignCashLoanResponseSchema):
    class Meta:
        ordered = True

    loan_credit_rating = fields.String()
    rounded_effective_interest = fields.Method("get_rounded_effective_interest")
    installment_payment_freq = fields.String()
    interest_payment_freq = fields.String()
    installment_length = fields.String()
    installment_length_unit = fields.Method('get_installment_length_unit')
    flat_interest = fields.Float()
    effective_interest = fields.Float()
    have_collateral = fields.Boolean()

class CampaignPartnerModelSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = CampaignPartner

    
    def get_partner(self, partner_id):
        try:
            campaign_partner_query = CampaignPartner.query.filter(CampaignPartner.is_deleted == False)
            campaign_partner_query = campaign_partner_query.filter(CampaignPartner.id == partner_id).first()

            data = {}
            data["partner"] = campaign_partner_query
            return data

        except Exception as e:
            GCPLogging.error(e, "Failed to get Campaign partner details")
            return data


class CampaignPartnerResponseSchema(ma.SQLAlchemySchema):
    class Meta:
        ordered = True
        model = CampaignPartner

    id = fields.String(data_key="partner_id")
    name = fields.String(data_key="partner_name")

class CampaignDetailsResponseSchema(BaseSchema):
    class Meta:
        ordered = True

    uuid = fields.String(data_key="campaign_uuid")
    campaign_name = fields.Method("get_campaign_name")
    category_id = fields.Integer()
    business_description = fields.String()
    revenue_description = fields.String()
    business_entity = fields.String()
    min_funding = fields.Float()
    max_funding = fields.Float()
    address = fields.String()
    status = fields.String(data_key="campaign_status")
    campaign_type = fields.Method("get_campaign_type")
    invoice = fields.Method("get_invoice")
    funded_percentage = fields.Float()
    total_investment = fields.Float()
    total_investors = fields.Integer(data_key = 'total_campaign_investors')
    active_date = fields.Method("get_active_date")
    campaign_start = fields.Method("get_campaign_start")
    campaign_end = fields.Method("get_campaign_end")
    campaign_time_remaining = fields.Method("get_campaign_time_remaining_forCampaign")
    head_coborrower_initial = fields.String()
    id = fields.Integer(data_key="loan_id")
    year_of_establishment = fields.Integer()
    form_of_business = fields.String()
    installment_length = fields.Integer()
    installment_length_unit = fields.Method("get_installment_length_unit")
    installment_payment_freq = fields.Method("get_installment_payment_freq")
    interest_payment_freq = fields.Method("get_interest_payment_freq")
    transaction_period_duration = fields.Integer()
    description = fields.String()
    partner = fields.Nested(CampaignPartnerResponseSchema)
    loan_purpose = fields.String()
    have_collateral = fields.Boolean()
    guarantee_collateral_description = fields.String()
    collateral_type = fields.Method("get_collateral_type")
    flat_interest = fields.Float()
    effective_interest = fields.Float()
    rounded_effective_interest = fields.Method("get_rounded_effective_interest")
    credit_rating = fields.Nested(CampaignCreditRatingResponseSchema, data_key="loan_credit_rating")
    cover = fields.String(data_key="cover_url")
    has_insurance = fields.Boolean()
    highlight_type = fields.Method("get_highlight_type")


    def get_campaign_time_remaining_forCampaign(self, obj):
        try:
            if obj["funded_percentage"] == 100:
                if obj["lang"] == "en":
                    return "campaign term is over"
                return "Berakhir"

            end_date = obj["campaign_end"].split(".")

            hours_remaining = (
                datetime.strptime(end_date[0], "%Y-%m-%dT%H:%M:%S") - datetime.now()
            ).total_seconds() / 60

            if hours_remaining < 0:
                if obj["lang"] == "en":
                    return "campaign term is over"
                return "Berakhir"

            if hours_remaining > 24:
                if obj["lang"] == "en":
                    return (
                        str((obj["campaign_end"] - datetime.now()).days)
                        + " Days remaining"
                    )
                return str((obj["campaign_end"] - datetime.now()).days) + " Hari lagi"

            if obj["lang"] == "en":
                return str(hours_remaining) + " Hours remaining"
            return str(hours_remaining) + " Jam lagi"
        except Exception as e:
            return None



class CampaignDetailsGenericAdditionalFieldsResponseSchema(CampaignDetailsResponseSchema):
    class Meta:
        ordered = True

    revenue_period_from = fields.Method("get_revenue_period_from")
    revenue_period_to = fields.Method("get_revenue_period_to")
    revenue = fields.Float()
    ebitda = fields.Float()
    nett_profit = fields.Float()
    debt_capital_period_by = fields.Method("get_debt_capital_period_by")
    capital = fields.Float()
    short_term_debt = fields.Float()
    long_term_debt = fields.Float()
    debt_equity_ratio = fields.Float()
    debt_asset_ratio = fields.Float()
    debt_service_ratio = fields.Float()

    def get_revenue_period_from(self, obj):
        try:
            start_date = obj['revenue_period_from'].split(".")
            date = datetime.strptime(start_date[0], "%Y-%m-%dT%H:%M:%S")
            return date.strftime("%Y-%m-%d %H:%M:%S")

        except Exception:
            return None

    def get_revenue_period_to(self, obj):
        try:
            start_date = obj['revenue_period_to'].split(".")
            date = datetime.strptime(start_date[0], "%Y-%m-%dT%H:%M:%S")
            return date.strftime("%Y-%m-%d %H:%M:%S")

        except Exception:
            return None

    def get_debt_capital_period_by(self, obj):
        try:
            start_date = obj['debt_capital_period_by'].split(".")
            date = datetime.strptime(start_date[0], "%Y-%m-%dT%H:%M:%S")
            return date.strftime("%Y-%m-%d %H:%M:%S")

        except Exception:
            return None


class CampaignOnlineMerchantHighlightSchema(ma.SQLAlchemySchema):
    class Meta:
        ordered = True

    merchant_sales_time_duration = fields.String()
    active_transaction_since = fields.String()
    merchant_sales_amount = fields.Float()
    platform_name = fields.String()
    merchant_sales_period_from = fields.String()
    merchant_sales_period_to = fields.String()
    merchant_platform_reputation = fields.String()
    loan_purpose_description = fields.String()
    transaction_period_duration = fields.String()


class CampaignDetailsOnlineMerchantAdditionalFieldsResponseSchema(
    CampaignDetailsGenericAdditionalFieldsResponseSchema):
    class Meta:
        ordered = True

    # platform_id = fields.Integer(data_key="platform")
    total_sales_time_month = fields.Method("get_total_sales_time_month")
    total_sales_time_year = fields.Method("get_total_sales_time_year")
    sales_amount = fields.Float()
    sales_period_from = fields.String()
    sales_period_to = fields.String()
    # merchant_reputation_id = fields.Integer(data_key="id")
    online_merchant_highlight = fields.Nested(CampaignOnlineMerchantHighlightSchema, many=False)

    def get_total_sales_time_month(self, obj):
        try:
            total_sales_time = obj["total_sales_time"]
            if total_sales_time:
                total_sales_time = total_sales_time.split(",")
                if len(total_sales_time) >= 2:
                    return total_sales_time[1]
            else:
                return "0"
        except Exception:
            return None

    def get_total_sales_time_year(self, obj):
        try:
            total_sales_time = obj["total_sales_time"]
            if total_sales_time:
                total_sales_time = total_sales_time.split(",")
                if len(total_sales_time) >= 2:
                    return total_sales_time[0]
            else:
                return "0"
        except Exception:
            return None


class AdminCampaignResponseModelSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        ordered = True

    have_collateral = fields.Boolean()
    campaign_name = fields.String()
    campaign_time_remaining = fields.String()
    id = fields.Integer()
    uuid = fields.String()
    slug = fields.String()
    campaign_type = fields.String()
    campaign_owner = fields.String()
    max_funding = fields.String()
    status = fields.String()
    total_investment = fields.Float()
    total_investors = fields.Integer()
    funded_percentage = fields.Float()
    created = fields.String()
    campaign_creator_name_anonymous = fields.String()
    campaign_creator_name = fields.String()
    institutional_lender_id = fields.Integer()
    has_insurance = fields.Boolean()


class CampaignPaymentListRequestSchema(ma.SQLAlchemySchema):
    class Meta:
        ordered = True
        model = CampaignPaymentList

    admin_fee = fields.Method("get_admin_fee")
    akseleran_fee = ma.auto_field()
    credit_insurance_fee = ma.auto_field()
    other_fee = ma.auto_field()

    def get_admin_fee(self, obj):
        return obj.channeling_fee + obj.upside_fee

    def get_payment_list(self, id):
        data = {}
        data["payment_list"] = ""
        campaign_payment_list_query = CampaignPaymentList.query.filter(CampaignPaymentList.is_deleted == False)
        campaign_payment_list_query = campaign_payment_list_query.filter(
            CampaignPaymentList.payment_status != "INITIALIZED")
        campaign_payment_list_query = campaign_payment_list_query.filter(
            CampaignPaymentList.campaign_id == id).all()

        data["payment_list"] = campaign_payment_list_query

        return data


class CampaignPaymentModelSchema(ma.SQLAlchemySchema):
    class Meta:
        ordered = True
        model = CampaignPaymentList

    admin_fee = fields.Method("get_admin_fee")
    akseleran_fee = ma.auto_field()
    credit_insurance_fee = ma.auto_field()
    other_fee = ma.auto_field()

    def get_admin_fee(self, obj):
        return obj.channeling_fee + obj.upside_fee

class AdminCampaignDetailsResponseSchema(BaseSchema):
    class Meta:
        ordered = True

    id = fields.Integer()
    uuid = fields.String(data_key="campaign_uuid")
    campaign_name = fields.String()
    business_description = fields.String()
    revenue_description = fields.String()
    business_entity = fields.String()
    category_id = fields.Integer()
    address = fields.String()
    phone = fields.String(data_key="phone_number")
    phone_number = fields.String(data_key="phone_number_alt")
    website = fields.String()
    fb_url = fields.String(data_key="facebook_url")
    twitter_url = fields.String()
    instagram_url = fields.String()
    min_funding = fields.Float()
    max_funding = fields.Float()
    minimum_investment = fields.Float()
    status = fields.String(data_key="campaign_status")
    campaign_type = fields.Method("get_campaign_type")
    invoice = fields.Method("get_invoice")
    funded_percentage = fields.Float()
    total_investment = fields.Float()
    total_campaign_investors = fields.Integer()
    active_date = fields.Method("get_active_date")
    campaign_start = fields.Method("get_campaign_start")
    campaign_end = fields.Method("get_campaign_end")
    campaign_time_remaining = fields.Method("get_campaign_time_remaining")
    is_featured = fields.Boolean()
    institutional_lender_id = fields.String()
    id = fields.Integer(data_key="loan_id", dump_only=True)
    year_of_establishment = fields.Integer()
    form_of_business = fields.String()
    installment_length = fields.Integer()
    installment_length_unit = fields.Method("get_installment_length_unit")
    installment_payment_freq = fields.Method("get_installment_payment_freq")
    interest_payment_freq = fields.Method("get_interest_payment_freq")
    transaction_period_duration = fields.Integer()
    description = fields.String()
    partner = fields.Nested(CampaignPartnerResponseSchema)
    loan_purpose = fields.String()
    have_collateral = fields.Boolean()
    guarantee_collateral_description = fields.String()
    collateral_type = fields.Method("get_collateral_type")
    flat_interest = fields.Float()
    effective_interest = fields.Float()
    rounded_effective_interest = fields.Method("get_rounded_effective_interest")
    credit_rating = fields.Nested(
        CampaignCreditRatingResponseSchema, data_key="loan_credit_rating")
    cover = fields.String(data_key="cover_url")
    has_insurance = fields.Boolean()
    highlight_type = fields.String()
    payment_list = fields.Nested(
        CampaignPaymentModelSchema, many=True)

from akcampaign.controllers.campaigns import get_featured_funding_details

class CampaignDetailsModelRequestSchema(ma.SQLAlchemyAutoSchema):

    def get_campaign_details(self, campaign_uuid):
        data = {}
        data['id'] = 0
        data['campaign'] = ""
        data['max_funding'] = 0
        campaign_query = Campaign.query.filter(Campaign.is_deleted == False)

        try:
            campaign_query = campaign_query.filter(Campaign.uuid == campaign_uuid).first()
            data["campaign"] = CampaignSchema().dump(campaign_query)
            data["campaign_details"] = CampaignDetailsSchema().dump(campaign_query.campaign_detail)
            data["id"] = campaign_query.id

            if campaign_query.loan_credit_rating_id is None:
                data["loan_credit_rating_id"] = None
            else:
                data["loan_credit_rating_id"] = campaign_query.loan_credit_rating_id

            data["max_funding"] = campaign_query.max_funding
            data["investments"] = get_featured_funding_details(campaign_query.campaign_investment, campaign_query.max_funding)

            if campaign_query.campaign_institutional_lender is None:
                data["campaign_institutional_lender"] = {
                    'institutional_lender_id': None
                }

            else:
                data["campaign_institutional_lender"] = CampaignInstitutionalLenderSchema().dump(campaign_query.campaign_institutional_lender)

        except Exception as e:
            GCPLogging.error(e, "Faied to get campaign details")
            return data

        return data
